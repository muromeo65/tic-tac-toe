Rails.application.routes.draw do
  resources :game, only: [:index] do
    collection do
      post 'movement',   to: 'game#movement'
      get 'load_spaces', to: 'game#load_spaces'
      get 'restart',     to: 'game#restart'
    end
  end
end
