require 'rails_helper'

RSpec.describe MakeMovement, type: :interactor do
  describe '.call' do
    let!(:params) { { option: 'a1' } }

    context 'when the player make his move' do
      subject { described_class.call(params) }

      it 'should return the machine movement' do
        expect(subject.machine_move).to be_present
      end
    end
  end
end
