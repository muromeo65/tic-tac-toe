document.addEventListener("DOMContentLoaded", function() {
  let board = document.getElementsByClassName('board');
  let spaces = document.getElementsByClassName('white-space');
  let restart = document.getElementsByClassName('restart');

  load_spaces();

  restart[0].addEventListener('click', () => {
    restart_game();
  });

  for (let i = 0; i < spaces.length; i++) {
    let current = spaces[i];
    current.addEventListener('click', function(){
      choose_option(current);
    });
  }
});

function load_spaces() {
  fetch('/game/load_spaces', {
    method: 'get',
    headers: { "Content-Type": "application/json" },
  }).then((resp) => resp.json()
    .then((data) => {
      write_movement(data);
    }
  ))
}

function choose_option(item) {
  let option = item.className.replace('white-space ', '').replace(/(mr-1)|(mr)/g, '').replace(/(\w+?-\w+?-\w+)|(\w+?-\w+)/g, '').trim(); // change to single regex

  if (item.classList.contains('x') || item.classList.contains('o')) {
    return;
  }

  if (document.getElementsByClassName('winner')[0]){
    return;
  }

  fetch('/game/movement', {
    method: 'post',
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ option: option })
  }).then((resp) => resp.json()
    .then((data) => {
      if (data['error']) {
        console.log(data['error']);
        return;
      }

      write_movement(data);
    }
  ))
}

function write_movement(data) {
  let board = data['board'];
  let winner = data['winner'];

  board.forEach((element, row) => {
    element.forEach((mark, column) => {
      if (mark == null) {
        return;
      }

      let position = get_position('board['+ row +']['+ column +']');
      let space = document.getElementsByClassName(position)[0];

      space.classList.add(mark);
    });
  });

  if (winner) {
    document.getElementsByClassName(winner)[0].classList.add('winner');
  }
}

function get_position(position) {
  if (position == 'board[0][0]') {
    return 'a1';
  }

  if (position == 'board[1][0]') {
    return 'a2';
  }

  if (position == 'board[2][0]') {
    return 'a3';
  }

  if (position == 'board[0][1]') {
    return 'b1';
  }

  if (position == 'board[1][1]') {
    return 'b2';
  }

  if (position == 'board[2][1]') {
    return 'b3';
  }

  if (position == 'board[0][2]') {
    return 'c1';
  }

  if (position == 'board[1][2]') {
    return 'c2';
  }

  if (position == 'board[2][2]') {
    return 'c3';
  }
}

function restart_game() {
  fetch('/game/restart', {
    method: 'get',
    headers: { "Content-Type": "application/json" },
  }).then((resp) => resp.json()
    .then((data) => {
      remove_movements(data['board']);
    }
  ))
}


function remove_movements(data) {
  data.forEach((element, row) => {
    element.forEach((mark, column) => {
      let position = get_position('board['+ row +']['+ column +']');
      let space = document.getElementsByClassName(position)[0];

      space.classList.remove('x');
      space.classList.remove('o');
      space.classList.remove('winner');
    });
  });
}
