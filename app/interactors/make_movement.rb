class MakeMovement
  include Interactor

  def call
    raise 'Movement cant be nil' if context.option.blank?

    save_players_move
    save_ais_move

    context.board = get_board
  rescue => e
    context.fail!(error: e.message)
  end

  private

  def save_players_move
    return if context.winner.present?

    player_mark = 'x'
    board = get_board

    position = get_position(board)
    raise 'Position not empty' if position.present?

    set_position(board, player_mark)

    context.winner = player_mark if has_won?(get_board, player_mark)
  end

  def save_ais_move
    return if context.winner.present?

    ai_mark = 'o'
    board = get_board

    nil_indexes = board.map { |b| b.map.with_index {|mark, i| i if mark.blank? }.compact }

    indexes     = nil_indexes.map.with_index { |v, i| i if v.present? }.compact
    return if indexes.blank?

    row         = random(indexes)
    space       = random(nil_indexes[row])

    board[row][space] = ai_mark

    redis.set('board', board)

    context.winner = ai_mark if has_won?(get_board, ai_mark)
  end

  def get_position(board)
    board[0][0] if context.option == 'a1'
    board[1][0] if context.option == 'a2'
    board[2][0] if context.option == 'a3'
    board[0][1] if context.option == 'b1'
    board[1][1] if context.option == 'b2'
    board[2][1] if context.option == 'b3'
    board[0][2] if context.option == 'c1'
    board[1][2] if context.option == 'c2'
    board[2][2] if context.option == 'c3'
  end

  def set_position(board, mark)
    board[0][0] = mark if context.option == 'a1'
    board[1][0] = mark if context.option == 'a2'
    board[2][0] = mark if context.option == 'a3'
    board[0][1] = mark if context.option == 'b1'
    board[1][1] = mark if context.option == 'b2'
    board[2][1] = mark if context.option == 'b3'
    board[0][2] = mark if context.option == 'c1'
    board[1][2] = mark if context.option == 'c2'
    board[2][2] = mark if context.option == 'c3'

    redis.set('board', board)
  end

  def redis
    @redis ||= Redis.new(host: 'localhost')
  end

  def get_board
    board = redis.get('board')

    if board.blank?
      redis.set('board', [[nil,nil,nil], [nil,nil,nil], [nil,nil,nil]])
      board = redis.get('board')
    end

    eval(board)
  end

  def random(array)
    array.sample(1).first
  end

  def has_won?(board, mark)
    win = false

    win = true if board[0].all? { |v| v == mark }
    win = true if board[1].all? { |v| v == mark }
    win = true if board[2].all? { |v| v == mark }

    win = true if [board[0][0], board[1][1], board[2][2]].all? { |v| v == mark }
    win = true if [board[0][2], board[1][1], board[2][0]].all? { |v| v == mark }

    win = true if [board[0][0], board[1][0], board[2][0]].all? { |v| v == mark }
    win = true if [board[0][2], board[1][2], board[2][2]].all? { |v| v == mark }

    win
  end
end
