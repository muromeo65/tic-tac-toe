class ApplicationController < ActionController::Base
  def redis
    @redis ||= Redis.new(host: 'localhost')
  end
end
