class GameController < ApplicationController
  skip_before_action :verify_authenticity_token # doesn't need any big protection to this simple webapp

  def index; end

  def movement
    result = MakeMovement.call(game_params)

    if result.success?
      render json: { board: result.board, winner: result.winner }
    else
      render json: { error: result.error }
    end
  end

  def load_spaces
    render json: { board: get_board }
  end

  def restart
    redis.set('board', [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])

    render json: { board: get_board }
  end

  private

  def get_board
    eval(redis.get('board'))
  end

  def game_params
    params.permit(:option)
  end
end
